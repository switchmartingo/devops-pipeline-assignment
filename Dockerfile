# Create image from maven image (https://hub.docker.com/_/maven/)
FROM maven:3-jdk-8

# Get repository files into the container
ADD . /devops

# Building the application..
WORKDIR /devops
RUN mvn install